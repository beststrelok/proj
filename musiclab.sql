-- phpMyAdmin SQL Dump
-- version 3.5.8.1deb1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Дек 10 2013 г., 09:34
-- Версия сервера: 5.5.34-0ubuntu0.13.04.1
-- Версия PHP: 5.4.9-4ubuntu2.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `musiclab`
--

-- --------------------------------------------------------

--
-- Структура таблицы `ratings`
--

CREATE TABLE IF NOT EXISTS `ratings` (
  `mark` int(3) NOT NULL,
  `comment` text NOT NULL,
  `time` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `song_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `ratings`
--

INSERT INTO `ratings` (`mark`, `comment`, `time`, `user_id`, `song_id`) VALUES
(5, 'awesome song!!!', '2013-12-06 14:13:56', 1, 3),
(1, 'bullshit', '2013-12-06 14:14:55', 1, 2),
(3, 'nice song, but not too much', '2013-12-06 14:15:05', 1, 4),
(5, 'good one!', '2013-12-06 14:15:45', 1, 1),
(1, 'stupid!!', '2013-12-06 14:16:05', 1, 5),
(5, 'love this artist', '2013-12-06 14:17:45', 2, 2),
(1, 'hate it', '2013-12-06 14:18:00', 2, 1),
(2, '', '2013-12-06 14:19:01', 2, 5),
(5, '', '2013-12-06 14:20:16', 3, 5),
(1, '', '2013-12-06 14:20:57', 3, 2),
(2, 'awful!!!', '2013-12-06 14:21:32', 3, 4),
(2, 'awful!!!', '2013-12-06 14:23:40', 3, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `songs`
--

CREATE TABLE IF NOT EXISTS `songs` (
  `song_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(64) NOT NULL,
  `artist` varchar(32) NOT NULL,
  `album` varchar(32) NOT NULL,
  `picture` varchar(64) NOT NULL,
  `rate` decimal(10,2) NOT NULL,
  `link` varchar(128) NOT NULL,
  PRIMARY KEY (`song_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Дамп данных таблицы `songs`
--

INSERT INTO `songs` (`song_id`, `title`, `artist`, `album`, `picture`, `rate`, `link`) VALUES
(1, 'Roar', 'Katy Perry', 'Prism', 'roar', 1.31, 'song_link_goes_here'),
(2, 'Undressed', 'Kim Cesarion', '', 'undressed', 3.45, 'song_link_goes_here'),
(3, 'Royals', 'Lorde', 'The Love Club EP', 'royals', 3.48, 'song_link_goes_here'),
(4, 'Sous L''oeil De L''ange', 'K-Maro', 'La Good Life', 'k-maro', 3.19, 'song_link_goes_here'),
(5, 'Profites', 'Costello', 'Prism', 'profites', 4.48, 'song_link_goes_here'),
(6, 'Empire Stete of Mind', 'Jay-Z', 'The Blueprint 3', 'empire_state', 4.00, 'song_link_goes_here'),
(7, 'Cheers', 'Rihanna', 'Loud', 'cheers', 3.19, 'song_link_goes_here'),
(8, 'A Milli', 'Lil Wayne', 'The Carter 3', 'milli', 2.45, 'song_link_goes_here');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(32) NOT NULL,
  `password` varchar(32) NOT NULL,
  `first_name` varchar(16) NOT NULL,
  `last_name` varchar(16) NOT NULL,
  `email` varchar(64) NOT NULL,
  `avatar` varchar(64) NOT NULL,
  `nickname` varchar(16) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`user_id`, `login`, `password`, `first_name`, `last_name`, `email`, `avatar`, `nickname`) VALUES
(1, 'beststrelok', 'd5cfbc9179a7f4e999a86d20bd0ef465', 'Ярослав', 'Здановский', 'beststrelok@gmail.com', 'avatar_title_goes_here', 'beststrelok'),
(2, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin', 'admin', 'admin@gmail.com', 'avatar_title_goes_here', 'admin'),
(3, 'user', 'ee11cbb19052e40b07aac0ca060c23ee', 'user', 'user', 'user@gmail.com', 'avatar_title_goes_here', 'user'),
(4, 'qweqwe', 'efe6398127928f1b2e9ef3207fb82663', 'qweqwe', 'qweqwe', 'qweqwe@gmail.com', '', 'qweqwe'),
(5, 'newone', '08b1d443ef0ab3677d2af8ef1afb1b28', 'newone', 'newone', 'newone@gmail.com', '', 'newone'),
(6, 'newtwo', 'c49b7cff7f50df183394de213acecec0', 'newtwo', 'newtwo', 'newtwo@gmail.com', '', 'newtwo'),
(7, 'newthree', 'f32866335231a089ad9dda79efeb9169', 'newthree', 'newthree', 'newthree@gmail.com', '', 'newthree'),
(8, 'fourone', '3afc2ad002a921d4a13720143f7904dd', 'fourone', 'fourone', 'fourone@gmail.com', '', 'fourone'),
(9, 'fiveone', 'a0d88ddbf513c846c8bbb23492ea71ec', 'fiveone', 'fiveone', 'fiveone@gmail.com', '', 'fiveone'),
(10, 'fiveone1', '98334cdc2197e9f286ed99039131c743', 'fiveone1', 'fiveone1', 'fiveone1@gmail.com', '', 'fiveone1'),
(11, 'fiveone2', '05e8a291fb13b7265b80ed8bf4e7da35', 'fiveone2', 'fiveone2', 'fiveone2@gmail.com', '', 'fiveone2'),
(12, 'fiveone3', 'ba9f121c6ea5ad55e91bd79e5b2b73ab', 'fiveone3', 'fiveone3', 'fiveone3@gmail.com', '', 'fiveone3'),
(13, 'fiveone4', '2df7f3d0b98842e160ab74ab2d125cdc', 'fiveone4', 'fiveone4', 'fiveone4@gmail.com', '', 'fiveone4'),
(14, 'sixone', '3d5c2be2a599a3aab4c8a4398fdc023c', 'sixone', 'sixone', 'sixone@gmail.com', '', 'sixone'),
(15, 'sevenone', '255b77f51405208ab418e8df4a4457db', 'sevenone', 'sevenone', 'sevenone@gmail.com', '', 'sevenone'),
(16, 'eightone', '909f039dff176406cb15381e1889c2d3', 'eightone', 'eightone', 'eightone@gmail.com', '', 'eightone');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
